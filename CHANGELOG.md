# ChangeLog

This provides a version history.

## History

| Version | Description |
|---------|------------ |
| 0.0.3 | Add security filter, add integration with Sonar, add unit tests
| 0.0.2 | Fix Sonar issues
| 0.0.1 | Initial Project implementation