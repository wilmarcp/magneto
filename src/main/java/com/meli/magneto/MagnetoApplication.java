package com.meli.magneto;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDynamoDBRepositories(basePackages = "com.meli.magneto.repository")
public class MagnetoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MagnetoApplication.class, args);
	}

}
