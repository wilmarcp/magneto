package com.meli.magneto.configuration;

import com.meli.magneto.service.validation.ChainValidation;
import com.meli.magneto.service.validation.GeneralValidator;
import com.meli.magneto.service.validation.HorizontalValidator;
import com.meli.magneto.service.validation.PrimaryDiagonalValidator;
import com.meli.magneto.service.validation.SecondaryDiagonalValidator;
import com.meli.magneto.service.validation.VerticalValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChainValidatorConfiguration {

    @Bean
    ChainValidation chainValidation() {
        return GeneralValidator
                .builder()
                .nextValidator(HorizontalValidator
                        .builder()
                        .nextValidator(VerticalValidator
                                .builder()
                                .nextValidator(PrimaryDiagonalValidator
                                        .builder()
                                        .nextValidator(SecondaryDiagonalValidator
                                                .builder()
                                                .nextValidator(null)
                                                .build())
                                        .build())
                                .build())
                        .build())
                .build();
    }
}
