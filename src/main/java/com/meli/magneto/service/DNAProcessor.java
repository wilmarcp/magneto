package com.meli.magneto.service;

import com.meli.magneto.model.type.Orientation;
import com.meli.magneto.model.type.OriginSequence;

import static com.meli.magneto.model.type.Orientation.HORIZONTAL;
import static com.meli.magneto.model.type.Orientation.PRIMARY_DIAGONAL;

import static com.meli.magneto.model.type.Orientation.SECONDARY_DIAGONAL;
import static com.meli.magneto.model.type.OriginSequence.SIDE;
import static com.meli.magneto.model.type.OriginSequence.TOP;
import static com.meli.magneto.model.type.OriginSequence.BOTTOM;

/**
 * Utility class process the DNA. Process the DNA in different directions. Ex: HORIZONTAL, VERTICAL, PRIMARY_DIAGONAL, SECONDARY_DIAGONAL
 */
public class DNAProcessor {

    public static final int SIZE_MUTANT_CHAIN = 4;    //Maximum size of the same char sequence to be identified as mutant
    public static final int MAX_SIZE_SEQUENCE = 1000; //Maximum size of the array sequence

    private DNAProcessor() {}

    /**
     * Process the DNA sequence in no oblique ways (HORIZONTAL, VERTICAL)
     * @param dnaSequence Array sequence to be processed and check if is a mutant
     * @param sequenceLength Array sequence length
     * @param orientation HORIZONTAL or VERTICAL
     * @return true if mutant, false if human
     */
    public static boolean validateNoObliqueDirection(char[][] dnaSequence, int sequenceLength, Orientation orientation) {
        int count = 0;

        boolean isHorizontal = orientation == HORIZONTAL;
        for(int i = 0; i < sequenceLength; i++) {
            char lastChar = getLastChar(dnaSequence, isHorizontal, i);
            for(int j = 0; j < sequenceLength; j++) {
                char currentChar = getCurrentChar(dnaSequence, isHorizontal, i, j);
                if(currentChar == lastChar) {
                    count++;
                    //If the dna sequence has SIZE_MUTANT_CHAIN consecutive characters so this sequence is marked as mutant
                    if (count == SIZE_MUTANT_CHAIN) {
                        return Boolean.TRUE;
                    }
                } else {
                    count = 1;
                }
                lastChar = currentChar;
            }
            count = 0;
        }
        return Boolean.FALSE;
    }

    private static char getLastChar(char[][] dnaSequence, boolean isHorizontal, int i) {
        return isHorizontal ? dnaSequence[i][0] : dnaSequence[0][i];
    }

    private static char getCurrentChar(char[][] dnaSequence, boolean isHorizontal, int i, int j) {
        return isHorizontal ? dnaSequence[i][j] : dnaSequence[j][i];
    }

    /**
     * Process the DNA sequence in oblique ways (PRIMARY_DIAGONAL, SECONDARY_DIAGONAL)
     * @param dnaSequence Array sequence to be processed and check if is a mutant
     * @param sequenceLength Array sequence length
     * @param diagonalFrom PRIMARY_DIAGONAL or SECONDARY_DIAGONAL
     * @param orientation SIDE, TOP, BOTTOM
     * @return true if mutant, false if human
     */
    public static boolean validateObliqueDirection(char[][] dnaSequence, int sequenceLength, OriginSequence diagonalFrom, Orientation orientation) {

        boolean shouldIncreaseIndexI = shouldIncreaseIndexI(orientation);

        //Final Index
        int finalI = getFinalI(orientation, sequenceLength);
        int finalJ = sequenceLength - 1;
        int initialIndex = getInitialIndex(orientation, diagonalFrom);
        int finalIndex = getFinalIndex(orientation, diagonalFrom, sequenceLength);

        for (int index = initialIndex; index <= finalIndex; index++) {

            //Initial Index
            int i = getFirstIndexI(sequenceLength, diagonalFrom, index);
            int j = getFirstIndexJ(diagonalFrom, index);

            char lastChar = dnaSequence[i][j];
            int count = 0;
            do {
                char currentChar = dnaSequence[i][j];
                if (lastChar == currentChar) {
                    count++;
                    if (count == SIZE_MUTANT_CHAIN) {
                        return Boolean.TRUE;
                    }
                } else {
                    count = 1;
                }
                lastChar = currentChar;
                i = shouldIncreaseIndexI ? i + 1 : i - 1;
                j++;
            } while (isThereMoreElementToProcess(orientation, finalI, finalJ, i, j));
        }
        return Boolean.FALSE;
    }

    /**
     * Check if there is more items to process in a line (horizontal, vertical or diagonal)
     * @param orientation The orientation that is processed. Ex. PRIMARY_DIAGONAL
     * @param finalI The final index row for the line that is being processed
     * @param finalJ The final index column for the line that is being processed
     * @param i The current row position in the matrix. If i = 3 means that the row 3 is being processed
     * @param j The current column position in the matrix. If j = 3 means that the column 3 is being processed
     * @return true if there are more elements to process
     */
    private static boolean isThereMoreElementToProcess(Orientation orientation, int finalI, int finalJ, int i, int j) {
        return (PRIMARY_DIAGONAL == orientation && i <= finalI && j <= finalJ) || (SECONDARY_DIAGONAL == orientation && i >= finalI && j <= finalJ);
    }

    private static int getFinalIndex(Orientation orientation, OriginSequence diagonalFrom, int sequenceLength) {
        int index = 0;
        if (PRIMARY_DIAGONAL == orientation || (SECONDARY_DIAGONAL == orientation && BOTTOM == diagonalFrom)) {
            index = sequenceLength - SIZE_MUTANT_CHAIN;
        } else if (SECONDARY_DIAGONAL == orientation && SIDE == diagonalFrom) {
            index = sequenceLength - 1;
        }

        return index;
    }

    private static int getInitialIndex(Orientation orientation, OriginSequence diagonalFrom) {
        int index = 0;
        if(PRIMARY_DIAGONAL == orientation || (SECONDARY_DIAGONAL == orientation && BOTTOM == diagonalFrom)) {
            index = 0;
        } else if (SECONDARY_DIAGONAL == orientation && SIDE == diagonalFrom) {
            index = SIZE_MUTANT_CHAIN - 1;
        }

        return index;
    }

    private static int getFinalI(Orientation orientation, int sequenceLength) {
        return PRIMARY_DIAGONAL == orientation ? sequenceLength - 1 : 0;
    }

    private static int getFirstIndexI(int sequenceLength, OriginSequence diagonalFrom, int indexReference) {
        int firstIndex;
        if (TOP == diagonalFrom) {
            firstIndex = 0;
        } else if (BOTTOM == diagonalFrom) {
            firstIndex = sequenceLength - 1;
        } else {
            firstIndex = indexReference;
        }
        return firstIndex;
    }

    private static int getFirstIndexJ(OriginSequence diagonalFrom, int indexReference) {
        int firstIndex;

        if (TOP == diagonalFrom || BOTTOM == diagonalFrom) {
            firstIndex = indexReference;
        } else {
            firstIndex = 0;
        }

        return firstIndex;
    }

    private static boolean shouldIncreaseIndexI(Orientation orientation) {
        return PRIMARY_DIAGONAL == orientation;
    }

}
