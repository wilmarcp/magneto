package com.meli.magneto.service.task;

import java.time.LocalDateTime;

import com.meli.magneto.service.StatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
@Slf4j
public class ScheduledStat {

    @Autowired
    private StatService statService;

    /**
     * This task is executed every middle night.
     * The target of this task is generate the mutant stat and persist it in data store.
     */
    @Scheduled(cron = "0 0 23 * * ?")
    public void generateStat() {
        log.info("Generate Stat task started at {}", LocalDateTime.now());
        statService.generateStat();
        log.info("Generate Stat task finished at {}", LocalDateTime.now());
    }
}
