package com.meli.magneto.service;

import java.math.BigDecimal;
import java.util.Optional;

import com.meli.magneto.model.MutantStat;
import com.meli.magneto.model.type.StatType;
import com.meli.magneto.repository.DNARepository;
import com.meli.magneto.repository.StatRepository;
import com.meli.magneto.service.exception.StatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatService {

    @Autowired
    DNARepository repository;

    @Autowired
    StatRepository statRepository;

    /**
     * Get the stats of the processed dna's
     * @return MutantStat has information about total humans, total mutants and th ratio mutants/humans
     */
    public MutantStat getStats() {
        long mutants = repository.countByIsMutant(true);
        long noMutants = repository.countByIsMutant(false);
        BigDecimal ratio = (noMutants > 0) ? new BigDecimal(mutants).divide(new BigDecimal(noMutants)) : BigDecimal.ZERO;
        return MutantStat.builder()
                .type(StatType.MUTANT.name())
                .countMutantDna(mutants)
                .countHumanDna(noMutants)
                .ratio(ratio.floatValue())
                .build();
    }

    /**
     * Get the stats of the processed dna's
     * @return MutantStat has information about total humans, total mutants and th ratio mutants/humans
     */
    public MutantStat getStatsOffline() throws StatException {
        Optional<MutantStat> stat = statRepository.findById(StatType.MUTANT.name());
        return stat.orElseThrow(StatException::new);
    }

    /**
     * Generate the Mutant stats and save in the data store
     */
    public void generateStat() {
        MutantStat mutantStat = getStats();
        mutantStat.setType(StatType.MUTANT.name());
        statRepository.save(mutantStat);
    }
}
