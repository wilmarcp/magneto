package com.meli.magneto.service.validation;

import java.util.Objects;

import com.meli.magneto.model.ValidationResponse;
import com.meli.magneto.service.DNAProcessor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

/**
 * Implement ChainValidation interface. This is the first step in the validation chain.
 * General validations:
 * - Size of the mutant sequence
 */
@Builder
@AllArgsConstructor
@Slf4j
public class GeneralValidator implements ChainValidation {

    ChainValidation nextValidator;

    @Override
    public ValidationResponse validate(char[][] dnaSequence) {
        log.info("Starting general validation on dna sequence");
        if (dnaSequence[0].length < DNAProcessor.SIZE_MUTANT_CHAIN) {
            return ValidationResponse.builder()
                    .code(ValidationResponse.Code.NO_MUTANT)
                    .message("Secuencia muy pequeña para ser mutante")
                    .build();
        } else if (dnaSequence[0].length > DNAProcessor.MAX_SIZE_SEQUENCE) {
            return ValidationResponse.builder()
                    .code(ValidationResponse.Code.UNDEFINED)
                    .message("Secuencia muy grande para ser identificada")
                    .build();
        }
        if (Objects.nonNull(nextValidator)) {
            return nextValidator.validate(dnaSequence);
        } else {
            return ValidationResponse.builder().build();
        }
    }
}
