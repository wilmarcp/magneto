package com.meli.magneto.service.validation;

import java.util.Objects;

import com.meli.magneto.model.type.Orientation;
import com.meli.magneto.model.type.OriginSequence;
import com.meli.magneto.model.ValidationResponse;
import com.meli.magneto.service.DNAProcessor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

/**
 * Implement ChainValidation interface. Business logic for identifying mutant sequence in the primary diagonal
 */
@Builder
@AllArgsConstructor
@Slf4j
public class PrimaryDiagonalValidator implements ChainValidation {

    ChainValidation nextValidator;

    @Override
    public ValidationResponse validate(char[][] dnaSequence) {
        log.info("Starting PrimaryDiagonal validation on dna sequence");
        if (DNAProcessor.validateObliqueDirection(dnaSequence, dnaSequence[0].length, OriginSequence.SIDE, Orientation.PRIMARY_DIAGONAL) ||
            DNAProcessor.validateObliqueDirection(dnaSequence, dnaSequence[0].length, OriginSequence.TOP, Orientation.PRIMARY_DIAGONAL)) {
            log.info("Validation - Mutant found in Primary Diagonal");
            return ValidationResponse
                    .builder()
                    .code(ValidationResponse.Code.MUTANT)
                    .message("Se ha encontrado una mutación en la Diagonal Principal")
                    .build();
        }
        if (Objects.nonNull(nextValidator)) {
            return nextValidator.validate(dnaSequence);
        } else {
            return ValidationResponse.builder().build();
        }
    }
}
