package com.meli.magneto.service.validation;

import com.meli.magneto.model.ValidationResponse;

/**
 * Definition of the chain validation. This interface should be implemented if new validation is needed
 */
public interface ChainValidation {

    /**
     * @param dnaSequence - Bidimensional array containing dna sequence
     * @return ValidationResponse has information about dna sequence validation. Indicate if sequence is mutant or not
     */
    ValidationResponse validate(char[][] dnaSequence);

}
