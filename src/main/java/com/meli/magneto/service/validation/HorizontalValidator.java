package com.meli.magneto.service.validation;

import java.util.Objects;

import com.meli.magneto.model.type.Orientation;
import com.meli.magneto.model.ValidationResponse;
import com.meli.magneto.service.DNAProcessor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

/**
 * Implement ChainValidation interface. Business logic for identifying horizontal mutant sequence
 */
@Builder
@AllArgsConstructor
@Slf4j
public class HorizontalValidator implements ChainValidation {

    ChainValidation nextValidator;

    @Override
    public ValidationResponse validate(char[][] dnaSequence) {
        log.info("Starting Horizontal validation on dna sequence");
        if (DNAProcessor.validateNoObliqueDirection(dnaSequence, dnaSequence[0].length, Orientation.HORIZONTAL)) {
            log.info("Validation - Mutant found in Horizontal");
            return ValidationResponse
                    .builder()
                    .code(ValidationResponse.Code.MUTANT)
                    .message("Se ha encontrado una mutación Horizontal")
                    .build();
        }
        if (Objects.nonNull(nextValidator)) {
            return nextValidator.validate(dnaSequence);
        } else {
            return ValidationResponse.builder().build();
        }
    }
}
