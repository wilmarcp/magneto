package com.meli.magneto.service;

import java.util.Arrays;
import java.util.regex.Pattern;

import com.meli.magneto.model.DNASequence;
import com.meli.magneto.model.ValidationResponse;
import com.meli.magneto.repository.DNARepository;
import com.meli.magneto.service.exception.MalformedDNAException;
import com.meli.magneto.service.validation.ChainValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class MutantService {

    @Autowired
    DNARepository repository;

    @Autowired
    ChainValidation validator;

    /**
     * Execute the validation chain to identify if the dna is mutant
     * @param dna to be validated
     * @return Validation information. The response has info about if is a mutant or human
     * @throws MalformedDNAException if the dna is malformed. Ex: if dna has invalid characters
     */
    public ValidationResponse isMutant(String[] dna) throws MalformedDNAException {
        ValidationResponse response = validator.validate(getSequence(dna));
        if (response.getCode() == ValidationResponse.Code.MUTANT || response.getCode() == ValidationResponse.Code.NO_MUTANT) {
            repository.save(DNASequence.builder()
                            .isMutant(response.isMutant())
                            .dna(Arrays.asList(dna))
                            .build());
        }
        return response;
    }

    /**
     * Convert string array to matrix sequence to be processed
     * @param dna Sequence as array of strings
     * @return char[][] array with the sequence to be processed
     * @throws MalformedDNAException if the dna is malformed. Ex: if dna has invalid characters
     */
    private static char[][] getSequence(String[] dna) throws MalformedDNAException {
        Pattern pattern = Pattern.compile("[ACGT]+");
        int sequenceLength = dna.length;
        char[][] dnaSequence = new char[sequenceLength][sequenceLength];

        for(int index = 0; index < dna.length; index++) {
            String partialSequence = dna[index];
            if (!isPartialSequenceValid(partialSequence, sequenceLength, pattern)) {
                throw new MalformedDNAException(ValidationResponse
                        .builder()
                        .code(ValidationResponse.Code.MALFORMED)
                        .message("La secuencia tiene alteraciones")
                        .build());
            } else {
                dnaSequence[index] = partialSequence.toCharArray();
            }
        }
        return dnaSequence;
    }

    /**
     * Check if a chunk of the sequence has the right length and right characters
     * @param partialSequence Chunk of the sequence to be checked
     * @param sequenceLength the length that chunk should have
     * @param pattern Pattern regex to validate the correct characters
     * @return true if chunk of the sequence is valid
     */
    private static boolean isPartialSequenceValid(String partialSequence, int sequenceLength, Pattern pattern) {
        boolean isValid = true;
        if (!hasCorrectSize(partialSequence, sequenceLength) || !hasCorrectElements(partialSequence, pattern)) {
            isValid = false;
        }
        return isValid;
    }

    private static boolean hasCorrectElements(String partialSequence, Pattern pattern) {
        return pattern.matcher(partialSequence).matches();
    }

    private static boolean hasCorrectSize(String partialSequence, int sequenceLength) {
        return StringUtils.hasText(partialSequence) && partialSequence.length() == sequenceLength;
    }

}
