package com.meli.magneto.service.exception;

import com.meli.magneto.model.ValidationResponse;
import lombok.Getter;

@Getter
public class MalformedDNAException extends Exception {

    transient ValidationResponse validationResponse;
    public MalformedDNAException (ValidationResponse validationResponse) {
        super(validationResponse.getMessage());
        this.validationResponse = validationResponse;
    }
}
