package com.meli.magneto.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "stats")
public class MutantStat {

    @DynamoDBHashKey
    private String type;

    @DynamoDBAttribute
    @JsonProperty("count_mutant_dna")
    private long countMutantDna;

    @DynamoDBAttribute
    @JsonProperty("count_human_dna")
    private long countHumanDna;

    @DynamoDBAttribute
    private float ratio;

}
