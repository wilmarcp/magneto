package com.meli.magneto.model.type;

/**
 * Enum type to identify the orientation in the process validation:
 * Ex. PRIMARY_DIAGONAL means that the process is taking every element from top-left to bottom-right as diagonal
 */
public enum Orientation {
    HORIZONTAL,
    VERTICAL,
    PRIMARY_DIAGONAL,
    SECONDARY_DIAGONAL
}
