package com.meli.magneto.model.type;

/**
 * Enum type to identify the origin from where init the process validation
 */
public enum OriginSequence {
    SIDE,
    TOP,
    BOTTOM
}
