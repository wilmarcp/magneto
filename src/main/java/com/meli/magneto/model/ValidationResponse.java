package com.meli.magneto.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ValidationResponse {
    @Builder.Default
    Code code = Code.NO_MUTANT;
    @Builder.Default
    String message = "No se  ha identificado mutaciones en esta secuencia";

    public boolean isMutant() {
        return Code.MUTANT == code;
    }

    public enum Code {
        UNDEFINED,
        MALFORMED,
        MUTANT,
        NO_MUTANT
    }
}
