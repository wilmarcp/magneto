package com.meli.magneto.repository;

import com.meli.magneto.model.MutantStat;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

@EnableScan
public interface StatRepository extends CrudRepository<MutantStat, String> {

}
