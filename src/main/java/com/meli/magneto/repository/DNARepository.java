package com.meli.magneto.repository;

import com.meli.magneto.model.DNASequence;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.repository.CrudRepository;

@EnableScan
public interface DNARepository extends CrudRepository<DNASequence, String> {

    @EnableScanCount
    long countByIsMutant(Boolean isMutant);
}