package com.meli.magneto.resource;

import javax.validation.Valid;

import com.meli.magneto.model.DNASequence;
import com.meli.magneto.model.ValidationResponse;
import com.meli.magneto.service.MutantService;
import com.meli.magneto.service.exception.MalformedDNAException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@Validated
@Slf4j
public class MutantResource {

    @Autowired
    private MutantService mutantService;

    @Operation(
            summary = "API Detector de mutante",
            description = "Api procesa secuencia de ADN para detectar si un humano o es mutante realizando ciertas validaciones",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Se identifica mutante",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = ValidationResponse.class),
                                            mediaType = "application/json"
                                    ),
                            }),
                    @ApiResponse(responseCode = "401", description = "Unauthorized",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = ValidationResponse.class),
                                            mediaType = "application/json"
                                    )
                            }),
                    @ApiResponse(responseCode = "403", description = "No se identifica mutante / Forbidden",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = ValidationResponse.class),
                                            mediaType = "application/json"
                                    )
                            }),
                    @ApiResponse(responseCode = "500", description = "Internal Server Error",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = String.class),
                                            mediaType = "application/json"
                                    )
                            })
            }
    )
    @PostMapping("/api/v1/mutant")
    public ResponseEntity<ValidationResponse> processDNA(@RequestBody @Valid DNASequence adnSequence) {
        log.info("Starting to process DNA");
        try {
            ValidationResponse response = mutantService.isMutant(adnSequence.getDna().toArray(new String[0]));
            if(response.isMutant()) {
                log.info("The process ended with Dna MUTANT ");
                return ResponseEntity.ok(response);
            } else {
                log.info("The process ended with Dna NO MUTANT ");
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
            }
        } catch(MalformedDNAException e) {
            log.error("Exception. The DNA was malformed");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getValidationResponse());
        }
    }

}
