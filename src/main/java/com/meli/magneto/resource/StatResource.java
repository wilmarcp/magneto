package com.meli.magneto.resource;

import com.meli.magneto.model.MutantStat;
import com.meli.magneto.model.ValidationResponse;
import com.meli.magneto.service.StatService;
import com.meli.magneto.service.exception.StatException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@AllArgsConstructor
@Validated
@Slf4j
public class StatResource {

    @Autowired
    private StatService statService;

    @Operation(
            summary = "Estadística de ADN procesados",
            description = "Obtiene las estadísticas de los ADN procesados. Indicador de Humanos, Mutantes y ratio (mutantes/humanos)",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Se obtiene la estadística satisfactoriamente",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = MutantStat.class),
                                            mediaType = "application/json"
                                    ),
                            }),
                    @ApiResponse(responseCode = "401", description = "Unauthorized",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = ValidationResponse.class),
                                            mediaType = "application/json"
                                    )
                            }),
                    @ApiResponse(responseCode = "403", description = "Forbidden",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = ValidationResponse.class),
                                            mediaType = "application/json"
                                    )
                            }),
                    @ApiResponse(responseCode = "500", description = "Internal Server Error",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = String.class),
                                            mediaType = "application/json"
                                    )
                            })
            }
    )
    @GetMapping("/api/v1/stats")
    public ResponseEntity<MutantStat> getStats() {
        log.info("Starting to get Stats");
        MutantStat response = statService.getStats();
        return ResponseEntity.ok(response);
    }

    @Operation(
            summary = "Estadística actualizada con la info del día anterior de ADN procesados",
            description = "Obtiene las estadísticas en modo offline (Actualizados con la información del día anterior) de los ADN procesados. Indicador de Humanos, Mutantes y ratio (mutantes/humanos)",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Se obtiene la estadística satisfactoriamente",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = MutantStat.class),
                                            mediaType = "application/json"
                                    ),
                            }),
                    @ApiResponse(responseCode = "401", description = "Unauthorized",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = ValidationResponse.class),
                                            mediaType = "application/json"
                                    )
                            }),
                    @ApiResponse(responseCode = "403", description = "Forbidden",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = ValidationResponse.class),
                                            mediaType = "application/json"
                                    )
                            }),
                    @ApiResponse(responseCode = "404", description = "No se encontró la estadística en la base de datos.",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = MutantStat.class),
                                            mediaType = "application/json"
                                    ),
                            }),
                    @ApiResponse(responseCode = "500", description = "Internal Server Error",
                            content = {
                                    @Content(
                                            schema = @Schema(implementation = String.class),
                                            mediaType = "application/json"
                                    )
                            })
            }
    )
    @GetMapping("/api/v1/stats/offline")
    public ResponseEntity<MutantStat> getStatsOffline() {
        log.info("Starting to get Stats offline");
        try {
            MutantStat response = statService.getStatsOffline();
            return ResponseEntity.ok(response);
        } catch (StatException e) {
            log.error("Error: Mutant stats not found");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error: stats not found", e);
        }
    }
}
