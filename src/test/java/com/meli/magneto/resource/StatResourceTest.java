package com.meli.magneto.resource;

import com.meli.magneto.model.MutantStat;
import com.meli.magneto.service.StatService;
import com.meli.magneto.service.exception.StatException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import static org.mockito.BDDMockito.given;

@SpringBootTest
class StatResourceTest {

    @Mock
    private StatService statService;

    @InjectMocks
    private StatResource statResource;

    @Test
    void getStatOffline() throws StatException {
        given(statService.getStatsOffline()).willReturn(MutantStat.builder().build());

        ResponseEntity<MutantStat> response =  statResource.getStatsOffline();
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getStatOfflineNotFound() throws StatException {
        given(statService.getStatsOffline()).willThrow(new StatException());
        Assertions.assertThrows(ResponseStatusException.class, () -> statResource.getStatsOffline());
    }

    @Test
    void getStat()  {
        given(statService.getStats()).willReturn(MutantStat
                .builder()
                .countMutantDna(5)
                .countHumanDna(5)
                .ratio(1)
                .build());
        ResponseEntity<MutantStat> response =  statResource.getStats();
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
