package com.meli.magneto.resource;

import java.util.Arrays;

import com.meli.magneto.model.DNASequence;
import com.meli.magneto.model.MutantStat;
import com.meli.magneto.model.ValidationResponse;
import com.meli.magneto.service.MutantService;
import com.meli.magneto.service.StatService;
import com.meli.magneto.service.exception.MalformedDNAException;
import com.meli.magneto.service.exception.StatException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class MutantResourceTest {

    @Mock
    private MutantService mutantService;

    @InjectMocks
    private MutantResource mutantResource;

    @Test
    void processDNAWithMutantResponse() throws MalformedDNAException {
        given(mutantService.isMutant(any())).willReturn(ValidationResponse.builder().code(ValidationResponse.Code.MUTANT).build());

        ResponseEntity<ValidationResponse> response =  mutantResource.processDNA(DNASequence.builder().dna(Arrays.asList("AA", "AA")).build());
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void processDNAWithNoMutantResponse() throws MalformedDNAException {
        given(mutantService.isMutant(any())).willReturn(ValidationResponse.builder().code(ValidationResponse.Code.NO_MUTANT).build());

        ResponseEntity<ValidationResponse> response =  mutantResource.processDNA(DNASequence.builder().dna(Arrays.asList("AA", "AA")).build());
        Assertions.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    @Test
    void processDNAWithMalformedDNA() throws MalformedDNAException {
        given(mutantService.isMutant(any())).willThrow(new MalformedDNAException(ValidationResponse.builder().code(ValidationResponse.Code.MALFORMED).build()));

        ResponseEntity<ValidationResponse> response =  mutantResource.processDNA(DNASequence.builder().dna(Arrays.asList("AA", "AA")).build());
        Assertions.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

}
