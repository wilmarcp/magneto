package com.meli.magneto.service.validation;

import com.meli.magneto.model.ValidationResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.BDDMockito.given;

@SpringBootTest
class VerticalValidatorTest {
    VerticalValidator verticalValidator;

    @Mock
    ChainValidation nextValidator;

    @BeforeEach
    void setup() {
        verticalValidator = new VerticalValidator(null);
    }

    @Test
    void validateMutantInFirstLine() {
        char[][] dnaSequence = {
                {'T','A','A','A','T'},
                {'T','G','A','A','T'},
                {'T','G','A','A','T'},
                {'T','G','A','A','T'},
                {'T','G','A','A','T'}
        };
        ValidationResponse response = verticalValidator.validate(dnaSequence);
        Assertions.assertEquals("Se ha encontrado una mutación Vertical", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.MUTANT, response.getCode());
    }

    @Test
    void validateMutantInLastLine() {
        char[][] dnaSequence = {
                {'T','G','A','A','G'},
                {'T','G','A','A','T'},
                {'C','G','A','A','T'},
                {'T','G','A','A','T'},
                {'A','A','A','A','T'},
        };
        ValidationResponse response = verticalValidator.validate(dnaSequence);
        Assertions.assertEquals("Se ha encontrado una mutación Vertical", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.MUTANT, response.getCode());
    }

    @Test
    void validateNoMutantWithoutNextValidator() {
        char[][] dnaSequence = {
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'C','C','C','C', 'C'},
                {'T','G','A','A', 'T'},
                {'A','A','G','A', 'T'},
        };
        ValidationResponse response = verticalValidator.validate(dnaSequence);
        Assertions.assertEquals("No se  ha identificado mutaciones en esta secuencia", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.NO_MUTANT, response.getCode());
    }

    @Test
    void validateNoMutantWithNextValidator() {
        char[][] dnaSequence = {
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'C','C','C','C', 'C'},
                {'T','G','A','A', 'T'},
                {'A','A','G','A', 'T'},
        };
        given(nextValidator.validate(dnaSequence)).willReturn(ValidationResponse.builder().build());
        verticalValidator = VerticalValidator.builder().nextValidator(nextValidator).build();

        ValidationResponse response = verticalValidator.validate(dnaSequence);
        Assertions.assertEquals("No se  ha identificado mutaciones en esta secuencia", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.NO_MUTANT, response.getCode());
    }
}
