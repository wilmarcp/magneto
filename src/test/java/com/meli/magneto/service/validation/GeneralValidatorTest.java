package com.meli.magneto.service.validation;

import com.meli.magneto.model.ValidationResponse;
import com.meli.magneto.service.DNAProcessor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.BDDMockito.given;

@SpringBootTest
class GeneralValidatorTest {

    GeneralValidator generalValidator;

    @Mock ChainValidation nextValidator;

    @BeforeEach
    public void setup() {
        generalValidator = new GeneralValidator(null);
    }

    @Test
    void validateNoMutantSmallSequence() {
        char[][] dnaSequence = {
                {'T','G','A'},
                {'T','G','A'},
                {'T','G','A'}
        };
        ValidationResponse response = generalValidator.validate(dnaSequence);
        Assertions.assertEquals("Secuencia muy pequeña para ser mutante", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.NO_MUTANT, response.getCode());
    }

    @Test
    void validateUndefinedLongSequence() {
        int sequenceSize = DNAProcessor.MAX_SIZE_SEQUENCE + 1;
        char[][] dnaSequence = new char[sequenceSize][sequenceSize];
        ValidationResponse response = generalValidator.validate(dnaSequence);
        Assertions.assertEquals("Secuencia muy grande para ser identificada", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.UNDEFINED, response.getCode());
    }

    @Test
    void validateNoMutantWithoutNextValidator() {
        char[][] dnaSequence = new char[5][5];
        ValidationResponse response = generalValidator.validate(dnaSequence);
        Assertions.assertEquals("No se  ha identificado mutaciones en esta secuencia", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.NO_MUTANT, response.getCode());
    }

    @Test
    void validateNoMutantWithNextValidator() {
        char[][] dnaSequence = new char[5][5];
        given(nextValidator.validate(dnaSequence)).willReturn(ValidationResponse.builder().build());
        generalValidator = GeneralValidator.builder().nextValidator(nextValidator).build();

        ValidationResponse response = generalValidator.validate(dnaSequence);
        Assertions.assertEquals("No se  ha identificado mutaciones en esta secuencia", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.NO_MUTANT, response.getCode());
    }
}
