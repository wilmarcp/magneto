package com.meli.magneto.service.validation;

import com.meli.magneto.model.ValidationResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.BDDMockito.given;

@SpringBootTest
class HorizontalValidatorTest {

    HorizontalValidator horizontalValidator;

    @Mock
    ChainValidation nextValidator;

    @BeforeEach
    public void setup() {
        horizontalValidator = new HorizontalValidator(null);
    }

    @Test
    void validateMutantInFirstLine() {
        char[][] dnaSequence = {
                {'A','A','A','A','T'},
                {'T','G','A','A','T'},
                {'T','G','A','A','T'},
                {'T','G','A','A','T'},
                {'T','G','A','A','T'}
        };
        ValidationResponse response = horizontalValidator.validate(dnaSequence);
        Assertions.assertEquals("Se ha encontrado una mutación Horizontal", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.MUTANT, response.getCode());
    }

    @Test
    void validateMutantInLastLine() {
        char[][] dnaSequence = {
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'T','A','A','A', 'A'},
        };
        ValidationResponse response = horizontalValidator.validate(dnaSequence);
        Assertions.assertEquals("Se ha encontrado una mutación Horizontal", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.MUTANT, response.getCode());
    }

    @Test
    void validateNoMutantWithoutNextValidator() {
        char[][] dnaSequence = {
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'A','A','G','A', 'T'},
        };
        ValidationResponse response = horizontalValidator.validate(dnaSequence);
        Assertions.assertEquals("No se  ha identificado mutaciones en esta secuencia", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.NO_MUTANT, response.getCode());
    }

    @Test
    void validateNoMutantWithNextValidator() {
        char[][] dnaSequence = {
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'A','A','G','A', 'T'},
        };
        given(nextValidator.validate(dnaSequence)).willReturn(ValidationResponse.builder().build());
        horizontalValidator = HorizontalValidator.builder().nextValidator(nextValidator).build();

        ValidationResponse response = horizontalValidator.validate(dnaSequence);
        Assertions.assertEquals("No se  ha identificado mutaciones en esta secuencia", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.NO_MUTANT, response.getCode());
    }
}
