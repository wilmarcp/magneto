package com.meli.magneto.service.validation;

import com.meli.magneto.model.ValidationResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.BDDMockito.given;

@SpringBootTest
class PrimaryDiagonalValidatorTest {
    PrimaryDiagonalValidator primaryDiagonalValidator;

    @Mock
    ChainValidation nextValidator;

    @BeforeEach
    public void setup() {
        primaryDiagonalValidator = new PrimaryDiagonalValidator(null);
    }

    @Test
    void validateMutantInFirstDiagonal() {
        char[][] dnaSequence = {
                {'T','A','A','A','T'},
                {'T','T','A','A','T'},
                {'T','G','T','A','T'},
                {'T','G','A','T','T'},
                {'T','G','A','A','G'}
        };
        ValidationResponse response = primaryDiagonalValidator.validate(dnaSequence);
        Assertions.assertEquals("Se ha encontrado una mutación en la Diagonal Principal", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.MUTANT, response.getCode());
    }

    @Test
    void validateMutantInLastDiagonal() {
        char[][] dnaSequence = {
                {'T','G','A','A','G'},
                {'T','A','A','A','T'},
                {'C','G','A','A','T'},
                {'T','G','A','A','T'},
                {'A','A','A','A','A'},
        };
        ValidationResponse response = primaryDiagonalValidator.validate(dnaSequence);
        Assertions.assertEquals("Se ha encontrado una mutación en la Diagonal Principal", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.MUTANT, response.getCode());
    }

    @Test
    void validateMutantInDiagonalFromTop() {
        char[][] dnaSequence = {
                {'T','G','A','A','G','A'},
                {'T','A','A','A','T','A'},
                {'C','G','T','A','T','A'},
                {'T','G','A','A','A','A'},
                {'A','A','A','A','A','A'},
                {'T','G','A','A','G','A'},
        };
        ValidationResponse response = primaryDiagonalValidator.validate(dnaSequence);
        Assertions.assertEquals("Se ha encontrado una mutación en la Diagonal Principal", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.MUTANT, response.getCode());
    }

    @Test
    void validateMutantInDiagonalFromSide() {
        char[][] dnaSequence = {
                {'T','G','A','A','G','A'},
                {'T','A','A','A','T','A'},
                {'C','A','T','G','T','A'},
                {'T','G','A','A','A','A'},
                {'A','A','A','A','A','A'},
                {'T','G','A','A','A','A'},
        };
        ValidationResponse response = primaryDiagonalValidator.validate(dnaSequence);
        Assertions.assertEquals("Se ha encontrado una mutación en la Diagonal Principal", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.MUTANT, response.getCode());
    }

    @Test
    void validateNoMutantWithoutNextValidator() {
        char[][] dnaSequence = {
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'C','C','C','C', 'C'},
                {'T','G','A','A', 'T'},
                {'A','A','G','A', 'T'},
        };
        ValidationResponse response = primaryDiagonalValidator.validate(dnaSequence);
        Assertions.assertEquals("No se  ha identificado mutaciones en esta secuencia", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.NO_MUTANT, response.getCode());
    }

    @Test
    void validateNoMutantWithNextValidator() {
        char[][] dnaSequence = {
                {'T','G','A','A', 'T'},
                {'T','G','A','A', 'T'},
                {'C','C','C','C', 'C'},
                {'T','G','A','A', 'T'},
                {'A','A','G','A', 'T'},
        };
        given(nextValidator.validate(dnaSequence)).willReturn(ValidationResponse.builder().build());
        primaryDiagonalValidator = PrimaryDiagonalValidator.builder().nextValidator(nextValidator).build();

        ValidationResponse response = primaryDiagonalValidator.validate(dnaSequence);
        Assertions.assertEquals("No se  ha identificado mutaciones en esta secuencia", response.getMessage());
        Assertions.assertEquals(ValidationResponse.Code.NO_MUTANT, response.getCode());
    }
}
