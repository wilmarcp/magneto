package com.meli.magneto.service.task;

import com.meli.magneto.service.StatService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
class ScheduledStatTest {

    @Mock
    private StatService statService;

    @InjectMocks
    private ScheduledStat scheduledStat;

    @Test
    void callTask() {
        scheduledStat.generateStat();
        verify(statService, times(1)).generateStat();
    }
}
