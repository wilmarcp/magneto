package com.meli.magneto.service;

import java.util.Optional;
import java.util.stream.Stream;

import com.meli.magneto.model.MutantStat;
import com.meli.magneto.model.ValidationResponse;
import com.meli.magneto.model.type.StatType;
import com.meli.magneto.repository.DNARepository;
import com.meli.magneto.repository.StatRepository;
import com.meli.magneto.service.exception.MalformedDNAException;
import com.meli.magneto.service.exception.StatException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
class StatServiceTest {

    @Mock
    DNARepository repository;

    @Mock
    StatRepository statRepository;

    @InjectMocks
    StatService statService;

    @ParameterizedTest
    @MethodSource("calculateRatioValues")
    void monthNames(long mutants, long noMutants, float ratio) {
        given(repository.countByIsMutant(true)).willReturn(mutants);
        given(repository.countByIsMutant(false)).willReturn(noMutants);
        MutantStat stat = statService.getStats();
        Assertions.assertEquals(ratio, stat.getRatio());
    }

    private static Stream<Arguments> calculateRatioValues() {
        return Stream.of(
                Arguments.arguments(5l, 0l, 0.0f),
                Arguments.arguments(0l, 5l, 0.0f),
                Arguments.arguments(2l, 5l, 0.4f)
        );
    }

    @Test
    void getStatsOfflineWithZeroRecordsFound() {
        given(statRepository.findById(StatType.MUTANT.name())).willReturn(Optional.empty());
        Assertions.assertThrows(StatException.class, () -> statService.getStatsOffline());
    }

    @Test
    void getStatsOfflineWithRecordStatFound() throws StatException {
        given(statRepository.findById(StatType.MUTANT.name())).willReturn(Optional.of(MutantStat
                .builder()
                .type(StatType.MUTANT.name())
                .ratio(0.5f)
                .build()));
        MutantStat stat = statService.getStatsOffline();
        Assertions.assertEquals(0.5f, stat.getRatio());
    }

    @Test
    void generateStat() throws MalformedDNAException {
        given(repository.countByIsMutant(true)).willReturn(2l);
        given(repository.countByIsMutant(false)).willReturn(5l);

        statService.generateStat();
        verify(statRepository, times(1)).save(any());
    }
}
