package com.meli.magneto.service;

import com.meli.magneto.model.ValidationResponse;
import com.meli.magneto.repository.DNARepository;
import com.meli.magneto.service.exception.MalformedDNAException;
import com.meli.magneto.service.validation.ChainValidation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
class MutantServiceTest {

    @Mock
    ChainValidation nextValidator;

    @Mock
    DNARepository repository;

    @InjectMocks
    MutantService mutantService;

    @Test
    void validateMutantMalformedWithWrongSize() {
        String[] dna = {"AGTCT","AGTCT","AGTCT"};
        Assertions.assertThrows(MalformedDNAException.class, () -> mutantService.isMutant(dna));
    }

    @Test
    void validateMutantMalformedWithWrongElements() {
        String[] dna = {"AGTCT","AGTCT","AGTCT","AGTCT","agct"};
        Assertions.assertThrows(MalformedDNAException.class, () -> mutantService.isMutant(dna));
    }

    @Test
    void validateNoMutant() throws MalformedDNAException {
        String[] dna = {"AGTCT","AGTCT","AGTCT","AGTCT","AGTCT"};
        given(nextValidator.validate(any())).willReturn(ValidationResponse.builder().build());

        ValidationResponse response = mutantService.isMutant(dna);
        Assertions.assertEquals(ValidationResponse.Code.NO_MUTANT, response.getCode());
        verify(repository, times(1)).save(any());
    }

    @Test
    void validateMutant() throws MalformedDNAException {
        String[] dna = {"AGTCT","AGTCT","AGTCT","AGTCT","AGTCT"};
        given(nextValidator.validate(any())).willReturn(ValidationResponse.builder().code(ValidationResponse.Code.MUTANT).build());

        ValidationResponse response = mutantService.isMutant(dna);
        Assertions.assertEquals(ValidationResponse.Code.MUTANT, response.getCode());
        verify(repository, times(1)).save(any());
    }

    @Test
    void validateCodeResponseUndefined() throws MalformedDNAException {
        String[] dna = {"AGTCT","AGTCT","AGTCT","AGTCT","AGTCT"};
        given(nextValidator.validate(any())).willReturn(ValidationResponse.builder().code(ValidationResponse.Code.UNDEFINED).build());

        ValidationResponse response = mutantService.isMutant(dna);
        Assertions.assertEquals(ValidationResponse.Code.UNDEFINED, response.getCode());
        verify(repository, never()).save(any());
    }
}
