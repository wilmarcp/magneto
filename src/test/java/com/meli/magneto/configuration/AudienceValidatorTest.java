package com.meli.magneto.configuration;

import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AudienceValidatorTest {

    AudienceValidator audienceValidator;

    @Test
    void validateAudienceFailure() {
        audienceValidator = new AudienceValidator("https://mutants.magneto.com");
        Jwt jwt = Jwt.withTokenValue("fake-token")
                .header("typ", "JWT")
                .header("alg", "none")
                .audience(Arrays.asList("https://mutants.magneto.com/wrong"))
                .build();
        OAuth2TokenValidatorResult validation = audienceValidator.validate(jwt);
        Assertions.assertTrue(validation.hasErrors());
    }

    @Test
    void validateAudienceSuccess() {
        audienceValidator = new AudienceValidator("https://mutants.magneto.com");
        Jwt jwt =Jwt.withTokenValue("fake-token")
                .header("typ", "JWT")
                .header("alg", "none")
                .audience(Arrays.asList("https://mutants.magneto.com"))
                .build();
        OAuth2TokenValidatorResult validation = audienceValidator.validate(jwt);
        Assertions.assertFalse(validation.hasErrors());
    }
}
