# Magneto API

Proyecto REST Api Magneto para procesar secuencias de ADN e identificar aquellas que sean mutantes

## Descipción
Esta API contiene los recursos para verificar si una secuencia de ADN corresponde a un Mutante o a un Humano y adicionalmente expone los servicios para obtener el ratio entre los mutantes y humanos de las secuencias de ADN procesadas.

## Documentación del API

- [Swagger](http://magneto-env.eba-iqz6umkp.us-west-2.elasticbeanstalk.com/swagger-ui/index.html)
- [Postman Collection](https://gitlab.com/wilmarcp/magneto/-/blob/develop/Magneto.postman_collection.json)

## Tecnologías implementadas
- SpringBoot 2.6.7
- Spring Data
- Spring Web
- Spring security
- AWS DynamoDB
- Elastic Beanstalk
- SwaggerUI (OpenApi)
- Lombok
- JUnit
- Mockito
- Gradle
- Java 11 Corretto

## Descripción de la implementación
- En la capa de negocio se implementó el patrón "Cadena de responsabilidades" con el fin de realizar la validación de la secuencia de ADN y verificar si corresponde a un mutante o un humano.
Cada paso de la cadena realiza una validación y al finalizar continua con el otro.
- Adicional a lo solicitado en el requerimiento, se implementó una tarea programada que se ejecuta cada noche a las 11 p.m
Esta tarea se encarga de generar las estadísticas y persistirla en una tabla adicional en la base de datos. También se habilitó un nuevo endpoint (GET /stats/offline) que retorna el ratio persistido en la noche anterior.
Este database report se implementó con el objetivo de tener el reporte generado cuando los registros de ADN hayan crecido lo suficiente y las consultas de las estadísticas empiecen a ser costosas.
- Se adicionó filtro se seguridad en el api. Para acceder a cada uno de los servicios es necesario enviar en los headers de la petición el token de seguridad.
Nota: La url para generar el token es enviado via e-mail.

## Pruebas unitarias y código limpio
[SONAR - Ver aquí informe de Cobertura](https://sonarcloud.io/summary/overall?id=wilmarcp_magneto)
La pruebas unitarias fueron implementadas con JUnit y Mockito con una cobertura sobre el 80%
Se integró mediante gitlab-ci el escaneo del código con SonarCloud
En este link se puede visualizar el estado del código y la cobertura [Sonar](https://sonarcloud.io/summary/overall?id=wilmarcp_magneto)

***

# Mejoras
- Test de integración
- Implementación de integración continua (Gitlab CI)
